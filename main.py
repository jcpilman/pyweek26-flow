# pyweek26 entry 
# FLOW
# main program

import os
import numpy as np
import pygame
import math
import time
from src.classlib import *
from src.helptext import *

# color pallet
grey = (200, 200, 200)
dkgrey = (150, 150, 150)
white = (255, 255, 255)
black = (0, 0, 0)
purple = (200, 0, 200)
ltblue = (100, 250, 240)
red = (255,0,0)
green = (0,255,0)
blue = (0,0,255)
dkblue = (0,0,128)
pink = (255,200,200)

datapath = 'data/'
#image files
buttonfile = 'upbutton.png'
tank1file = 'tank2.png'
fillpipefile = 'infillpipe.png'
continuebuttonfile = 'continue.png'
wolffile = 'wolf.png'
handyfile = 'handy5.png'
savedgamefile = 'savedgame.txt'

# sounds
backgroundmusicfile = 'Bolero.ogg'
wolfhowlfile = 'wolf6.ogg'

# screen size width and height
screen_size = (1028, 768)
scr_w = screen_size[0]
scr_h = screen_size[1]


def getsavedgame(savedgamefile):
    if not os.path.exists(savedgamefile):
        return 0
    with open(savedgamefile, 'r') as f:
        x = f.readline()
        if x:
            return int(x)
        return 0

def savegame(wherearewenow):
    with open(savedgamefile, 'w') as f:
        f.write(str(wherearewenow))

def main():

    savedgame = getsavedgame(savedgamefile)

    # setup timing
    clock = pygame.time.Clock()
    FRAMES_PER_SECOND = 30
    deltat = clock.tick(FRAMES_PER_SECOND)

    # initialize the game surface
    pygame.init()
    screen = pygame.display.set_mode(screen_size)
    pygame.display.set_caption('wolf')
    screen.fill(grey)

    # set up a couple PID loops
    FIC100 = PID('FIC100', 0, 250, 'GPM', 'Flow')
    LIC100 = PID('LIC100', 0, 100, '%', 'Level')
    # set mv for initial testing
    FIC100.mv = 0

    # setup a continue button
    continuebutton = Button(screen, datapath + continuebuttonfile, b_width = 125, b_height = 50)

    # setup a timing loop to use 1 sec as control basis
    timelast = 0
    timenow = time.time()
    timelast = timenow
    timestart = timenow
    timespan = timenow - timestart
    timeincrement = 1.0
    timeshard = 0
    mouse_loc = 0, 0

    running = True

    # background music
    music = False


    if running and savedgame == 0:
        # flow wolf
        alphawolf = Button(screen, datapath + wolffile, 'up', b_width = 500, b_height = 300)
        alphawolf.rect.x = 10
        alphawolf.rect.y = 1
        alphawolf.draw()
        pygame.display.flip()
        # move wolf to middle of screen
        xspeed = 1

    while running and savedgame == 0:
        # screen.fill(grey, alphawolf.rect)
        alphawolf.rect = alphawolf.rect.move([xspeed,0])
        screen.blit(screen, alphawolf.rect)
        pygame.display.flip()
        if alphawolf.rect.x >= 25:
            savedgame += 1
            savegame(savedgame)
            clickhelp(screen_size, 0)
            pygame.display.flip()

        # keyboard and mouse
        events = pygame.event.get()
        for event in events:
            if event.type == pygame.MOUSEBUTTONDOWN:
                # check for button pick
                mouse_loc = pygame.mouse.get_pos()
                # check for next button being pressed
                savedgame += 1
            if event.type == pygame.KEYDOWN:
                # when ESC is pressed end game
                if event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
                    running = False
                elif event.type == pygame.QUIT:
                    running = False
        pygame.display.flip()

    if running and savedgame == 1 and music:
        pygame.mixer.music.load(datapath + wolfhowlfile)
        pygame.mixer.music.set_endevent()
        pygame.mixer.music.play(0)
    if running and savedgame == 1:
        pygame.time.wait(4000)

    if music:
        pygame.mixer.music.load(datapath + backgroundmusicfile)
        pygame.mixer.music.play(-1)

    if running:
        # handy
        handycontext = 1
        handy = Button(screen, datapath + handyfile, 'up', b_width = 38, b_height = 33)
        handy.rectx = 1
        handy.recty = 1

    if running and savedgame == 1:
        # intro
        cleantheworld(screen)
        clickhelp(screen_size, 1)
        handy.draw()

    while running and savedgame == 1:
        # keyboard and mouse
        events = pygame.event.get()
        for event in events:
            if event.type == pygame.MOUSEBUTTONDOWN:
                # check for button pick
                mouse_loc = pygame.mouse.get_pos()
                askforhelp = handy.checkcollide(mouse_loc)
                if askforhelp:
                    handycontext = handyhelp(screen, handycontext)
                else:                
                    # check for next button being pressed
                    savedgame += 1
                    savegame(savedgame)

            if event.type == pygame.KEYDOWN:
                # when ESC is pressed end game
                if event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
                    running = False
                elif event.type == pygame.QUIT:
                    running = False
        pygame.display.flip()

    if savedgame == 2:
        # practice with the thumbwheel
        cleantheworld(screen)
        clickhelp(screen_size, 2)
        # initialize a thumbwheel
        # screen_size = (1028, 768)
        tw1x = int(screen_size[0]/2)
        tw1y =  int(screen_size[1]/2)
        tw1 = Thumbwheel(screen, datapath + buttonfile, (tw1x, tw1y), (0, 100), 100, '%')
        handy.rect.x = tw1x - 50
        handy.rect.y = tw1y + 25
        handy.draw()
        handycontext = 2

    while running and savedgame == 2:
        # keyboard and mouse events
        events = pygame.event.get()
        for event in events:
            if event.type == pygame.MOUSEBUTTONDOWN:
                # check for button pick
                mouse_loc = pygame.mouse.get_pos()
                # check for a button being pressed
                # the return value is an int
                FIC100.mv = tw1.buttoncheck(mouse_loc, FIC100.mv)
                if FIC100.mv == 99:
                    # success
                    savedgame += 1
                    savegame(savedgame)
                askforhelp = handy.checkcollide(mouse_loc)
                if askforhelp:
                    handycontext = handyhelp(screen, handycontext)

            if event.type == pygame.KEYDOWN:
                # when ESC is pressed end game
                if event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
                    running = False
                elif event.type == pygame.QUIT:
                    running = False

        pygame.display.flip()

    if running and savedgame == 3:
        # practice done
        cleantheworld(screen)
        # positive reward
        clickhelp(screen_size, 3)
        continuebutton.rect.x = 200
        continuebutton.rect.y = 500
        continuebutton.draw()
        pygame.display.flip()

    while running and savedgame == 3:
        # wait for click to next
        # keyboard and mouse
        events = pygame.event.get()
        for event in events:
            if event.type == pygame.MOUSEBUTTONDOWN:
                # check for button pick
                mouse_loc = pygame.mouse.get_pos()
                # check for next button being pressed
                if continuebutton.checkcollide(mouse_loc):
                    savedgame += 1
                    savegame(savedgame)
            if event.type == pygame.KEYDOWN:
                # when ESC is pressed end game
                if event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
                    running = False
                elif event.type == pygame.QUIT:
                    running = False

    if running and savedgame == 4:
        # open the valve and drain the tank
        cleantheworld(screen)
        clickhelp(screen_size, 4)
        #instantiate a tank
        tk1 = Tank(screen, datapath + tank1file, (10, 370))
        tk1.draw()
        # fill pipe
        tk2 = Tank(screen, datapath + fillpipefile, (-40, 330))
        tk2.draw()
        # instantiate water flow
        inflow = Bargraph(screen, (89, 361), (10, 40), red, grey)
        outflow = Bargraph(screen, (640, 628), (10, 40), red, grey)
        inflow.draw(100)
        outflow.draw(100)
        inflowrate = 0

        # start with partial tank level
        LIC100.pv = 80
        LIC100.sp = 25
        FIC100.mv = 0
        # instantiate a bargraph
        bg1 = Bargraph(screen, (48, 370), (150, 270), red, grey)
        bg1.draw(LIC100.pv)
        # instantiate a numeric value display for level
        nv1 = NumericVariable(screen, (80, 660), '%', '')
        nv1.draw(LIC100.pv)

        # instantiate a numeric value display for inflow
        nv2 = NumericVariable(screen, (10, 280), 'GPM', '')
        nv2.draw(inflowrate)

        #instantiate a button set
        tw1 = Thumbwheel(screen, datapath + buttonfile, (440, 650), (0, 100), 10, '%')
        # setup trend for level
        trend1 = Trend(trscreen = screen, loop = LIC100, 
            trlocation = (scr_w - 450, 50), trsize = (400, 250))

        handy.rect.x =  10 + 10
        handy.rect.y = 280 - 20
        handy.draw()
        handycontext = 40

        pygame.display.flip()
        
    while running and savedgame == 4:
        # keyboard and mouse
        events = pygame.event.get()
        for event in events:
            if event.type == pygame.MOUSEBUTTONDOWN:
                # check for button pick
                mouse_loc = pygame.mouse.get_pos()
                # check for a button being pressed
                # the return value is an int
                FIC100.mv = tw1.buttoncheck(mouse_loc, FIC100.mv)
                LIC100.mv = FIC100.mv
                askforhelp = handy.checkcollide(mouse_loc)
                if askforhelp:
                    handycontext = handyhelp(screen, handycontext)
                    handy.clear()
                    if handycontext == 40:
                        # by infill
                        handy.rect.x =  10 + 10
                        handy.rect.y = 280 - 20
                    elif handycontext == 41:
                        # by thumbwheel
                        handy.rect.x = 440 - 40
                        handy.rect.y = 650 + 40
                    elif handycontext == 42:
                        # by trend
                        # trlocation = (scr_w - 450, 50), trsize = (400, 250)
                        handy.rect.x = (scr_w - 450) - 40
                        handy.rect.y = 50 + 250 / 2
                    elif handycontext == 43:
                        # by red
                        # ptext.draw('SP', (self.location[0] + 90, self.location[1] - 20), color="green")
                        # ptext.draw('PV', (self.location[0] + 180, self.location[1] - 20), color="red")
                        # ptext.draw('Output', (self.location[0] + 270, self.location[1] - 20), color="white")
                        handy.rect.x = (scr_w - 450) + 180 - 40
                        handy.rect.y = 50 - 40
                    elif handycontext == 44:
                        # by white
                        handy.rect.x = (scr_w - 450) + 270 - 40
                        handy.rect.y = 50 - 40
                    elif handycontext == 45:
                        # by green
                        handy.rect.x = (scr_w - 450) + 90 - 40
                        handy.rect.y = 50 - 40
                    else:
                        handycontext = 40
                        handy.rect.x =  10 + 10
                        handy.rect.y = 280 - 40
                    handy.draw()

            if event.type == pygame.KEYDOWN:
                # when ESC is pressed end game
                if event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
                    running = False
                elif event.type == pygame.QUIT:
                    running = False

        timeincrement = 1.0
        timenow = time.time()
        timedelta = timenow - timelast

        # every second, update control functions and screen
        if timedelta >= timeincrement:
            # watch for time elements of time which accumulate
            timeshard = timedelta - timeincrement
            timedelta = timeincrement
            timelast = timenow - timeshard

            # calculate the inflow
            # the flow runs at 40 GPM for 20 seconds on 20 seconds off
            majorperiod = timenow % 20
            if majorperiod <= 10:
                inflowrate = 40
            else:
                inflowrate = 0
            nv2.draw(inflowrate)

            # calculate the level
            LIC100.pv = tanklevelcalc(LIC100.pv, inflowrate, FIC100.pv)
            # calculate the outflow
            FIC100.pv = flowratecalc(LIC100.pv, FIC100.mv)

            bg1.draw(LIC100.pv)
            nv1.draw(r(LIC100.pv, 1))
            if inflowrate > 2:
                inflow.draw(100)
            else:
                inflow.draw(0)
            if FIC100.pv > 2:
                outflow.draw(100)
            else:
                outflow.draw(0)
            # update trend
            trend1.penupdate()

            # check conditions for completing level
            if LIC100.pv < 25 and FIC100.mv == 0:
                savedgame += 1
                savegame(savedgame)
        timespan = timenow - timestart
        # pygame.display.update()
        pygame.display.flip()
#
    if running and savedgame == 5:
        # tank drain done
        cleantheworld(screen)
        # positive reward
        clickhelp(screen_size, 5)
        continuebutton.rect.x = 200
        continuebutton.rect.y = 500
        continuebutton.draw()
        pygame.display.flip()

    while running and savedgame == 5:
        # wait for click to next
        # keyboard and mouse
        events = pygame.event.get()
        for event in events:
            if event.type == pygame.MOUSEBUTTONDOWN:
                # check for button pick
                mouse_loc = pygame.mouse.get_pos()
                # check for next button being pressed
                if continuebutton.checkcollide(mouse_loc):
                    savedgame += 1
                    savegame(savedgame)
            if event.type == pygame.KEYDOWN:
                # when ESC is pressed end game
                if event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
                    running = False
                elif event.type == pygame.QUIT:
                    running = False

    if running and savedgame == 6:
        # control the tank level in auto
        cleantheworld(screen)
        clickhelp(screen_size, 6)
        #instantiate a tank
        tk1 = Tank(screen, datapath + tank1file, (10, 370))
        tk1.draw()
        # fill pipe
        tk2 = Tank(screen, datapath + fillpipefile, (-40, 330))
        tk2.draw()
        # instantiate water flow
        inflow = Bargraph(screen, (89, 361), (10, 40), red, grey)
        outflow = Bargraph(screen, (640, 628), (10, 40), red, grey)
        inflow.draw(100)
        outflow.draw(100)
        inflowrate = 0

        # start with partial tank level
        LIC100.pv = 20
        LIC100.sp = 0
        FIC100.mv = 0
        # instantiate a bargraph
        bg1 = Bargraph(screen, (48, 370), (150, 270), red, grey)
        bg1.draw(LIC100.pv)
        # instantiate a numeric value display for level
        nv1 = NumericVariable(screen, (80, 660), '%')
        nv1.draw(LIC100.pv)

        # instantiate a numeric value display for inflow
        nv2 = NumericVariable(screen, (10, 280), 'GPM')
        nv2.draw(inflowrate)

        #instantiate a button set for sp
        tw1 = Thumbwheel(screen, datapath + buttonfile, (640, 325), (0, 100), 100, '%', 'SP')
        #instantiate a button set for P
        P1 = Thumbwheel(screen, datapath + buttonfile, (640, 400), (0, 100), 100, 'gain', 'P')
        #instantiate a button set for I
        I1 = Thumbwheel(screen, datapath + buttonfile, (640, 475), (0, 100), 100, 'rps', 'I')
        # setup trend for level
        trend1 = Trend(trscreen = screen, loop = LIC100, 
            trlocation = (scr_w - 450, 50), trsize = (400, 250))
        pygame.display.flip()
        
    while running and savedgame == 6:
        # keyboard and mouse
        events = pygame.event.get()
        for event in events:
            if event.type == pygame.MOUSEBUTTONDOWN:
                # check for button pick
                mouse_loc = pygame.mouse.get_pos()
                # check for a button being pressed
                # the return value is an int
                LIC100.sp = tw1.buttoncheck(mouse_loc, LIC100.sp)
                LIC100.P = P1.buttoncheck(mouse_loc, LIC100.P)
                LIC100.I = I1.buttoncheck(mouse_loc, LIC100.I)

            if event.type == pygame.KEYDOWN:
                # when ESC is pressed end game
                if event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
                    running = False
                elif event.type == pygame.QUIT:
                    running = False

        timeincrement = 1.0
        timenow = time.time()
        timedelta = timenow - timelast

        # every second, update control functions and screen
        if timedelta >= timeincrement:
            # watch for time elements of time which accumulate
            timeshard = timedelta - timeincrement
            timedelta = timeincrement
            timelast = timenow - timeshard

            # calculate the inflow
            # the flow runs at 40 GPM for 20 seconds on 20 seconds off
            inflowrate = calculate_inflowrate()
            nv2.draw(r(inflowrate, 1))

            # calculate the level
            LIC100.pv = tanklevelcalc(LIC100.pv, inflowrate, FIC100.pv)
            # calculate the valve position
            LIC100.PID_calc()

            # calculate the outflow
            FIC100.mv = LIC100.mv
            FIC100.pv = flowratecalc(LIC100.pv, FIC100.mv)

            bg1.draw(LIC100.pv)
            nv1.draw(r(LIC100.pv, 1))
            if inflowrate > 2:
                inflow.draw(100)
            else:
                inflow.draw(0)
            if FIC100.pv > 2:
                outflow.draw(100)
            else:
                outflow.draw(0)
            # update trend
            trend1.penupdate()

            # check conditions for completing level
            lastpart = int(len(trend1.pvval) * 1 / 4)
            pvstd = r(np.std(trend1.pvval[lastpart:]))
            mvstd = r(np.std(trend1.mvval[lastpart:]))
            print(pvstd, mvstd)
            if LIC100.sp == 50 and pvstd < 2 and mvstd < 2:
                savedgame += 1
        timespan = timenow - timestart
        # pygame.display.update()
        pygame.display.flip()

    if running and savedgame > 6:
        # end of game
        cleantheworld(screen)
        clickhelp(screen_size, 99)
        pygame.display.flip()

    while running:
        # wait for click to next
        # keyboard and mouse
        events = pygame.event.get()
        for event in events:
            if event.type == pygame.MOUSEBUTTONDOWN:
                # check for button pick
                running = False
            if event.type == pygame.KEYDOWN:
                # when ESC is pressed end game
                if event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
                    running = False
                elif event.type == pygame.QUIT:
                    running = False

    pygame.quit()

def flowratecalc(level, opening):
    # flowrate depends on level and valve opening
    # at 100% level and 100% valve opening the 
    # flow is 100 GPM
    return 200 * level * opening / 10000

def tanklevelcalc(volume = 60, inflow = 10, outflow = 20):
    # the tank level changes based on flows
    # calculate once per second
    # volume in gallons
    # flows in GPM
    newvol = volume + (inflow - outflow) / 60
    newvol = between(0, newvol, 100)
    return newvol

def calculate_inflowrate():
    # the tank inflow rate will be a sinewave
    return sinewave(20, 40)

def sinewave(period = 20, amplitude = 100):
    # a sinewave with period p seconds and amplitude a
    # running from the start of today
    # number of seconds since midnight
    daystart = time.gmtime()
    x =  3600 * daystart[3] + 60 * daystart[4] + daystart[5]

    return amplitude * (1 + math.sin(x * 2 * math.pi / period))

def cleantheworld(screen):
    screen.fill(grey)
    pygame.display.flip()




if __name__ == "__main__":
    main()
    # print('main.py is a Library')
