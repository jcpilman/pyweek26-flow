The Big Board
===============

Entry in PyWeek #22  <http://www.pyweek.org/22/>
URL: https://www.pyweek.org/e/xxxxxxxxxxxxxx/
Team: Question Time
Members: John Pilman (typhonic)
License: see LICENSE.txt


DEPENDENCIES:
-------------

  Python 3.5+:     http://www.python.org/
  PyGame 1.9+:     http://www.pygame.org/


Running the Game
----------------

Open a terminal / console and "cd" to the game directory and run:

  python3 run_game.py


How to Play the Game
--------------------

