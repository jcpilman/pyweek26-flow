# pyweek26 entry 
# FLOW
# class library

import pygame
import math
import time
from . import ptext

# color pallet
grey = (200, 200, 200)
dkgrey = (150, 150, 150)
white = (255, 255, 255)
black = (0, 0, 0)
purple = (200, 0, 200)
ltblue = (100, 250, 240)
red = (255,0,0)
green = (0,255,0)
blue = (0,0,255)
dkblue = (0,0,128)
pink = (255,200,200)

datapath = 'data/'
# screen size width and height
screen_size = (1028, 768)
scr_w = screen_size[0]
scr_h = screen_size[1]

b_width = 24
b_height = 24

class Bargraph:
    # this will initially be used for tank level
    def __init__(self, screen, location, size, fgcolor, bgcolor):
        self.screen = screen
        self.size = size
        self.width = size[0]
        self.height = size[1] 
        self.location = location
        self.fgcolor = fgcolor
        self.bgcolor = bgcolor

    def draw(self, value):
        # value is a percentage of height
        # 100 is full height
        # 0 is at the bottom
        # bar grows up
        # bottom visible part of bar is fgcolor
        # top part is bgcolor

        # first draw the background full height
        xs = self.location[0]
        ys = self.location[1]
        xe = self.size[0]
        ye = self.size[1]
        pygame.draw.rect(self.screen, self.bgcolor, (xs,ys, xe, ye), 0)

        # limit the value to 0 - 100
        value = between(0, value, 100)
        # next draw the foreground
        xs = self.location[0]
        ys = int(self.location[1] + ((100 - value) / 100) * self.size[1])
        xe = self.size[0]
        ye = int(self.size[1] - ((100 - value) / 100) * self.size[1])
        pygame.draw.rect(self.screen, self.fgcolor, (xs,ys, xe, ye), 0)

class Trend:
    # initialize the loacation of the trend 
    # remember 0, 0 is upper left
    def __init__(self, trscreen, loop, trlocation, trsize):
        self.screen = trscreen
        # location x, y
        self.location = trlocation
        # size width, height
        self.size = trsize
        # initialize trend value lists
        self.pvval = [0] * 60
        self.spval = [0] * 60
        self.mvval = [0] * 60
        self.gridspace = int(trsize[1]/10)
        self.loop = loop
        ptext.draw(loop.desc, (self.location[0], self.location[1] - 20), color="black")
        ptext.draw('SP', (self.location[0] + 90, self.location[1] - 20), color="green")
        ptext.draw('PV', (self.location[0] + 180, self.location[1] - 20), color="red")
        ptext.draw('Output', (self.location[0] + 270, self.location[1] - 20), color="white")

    def grid(self):
        gridcolor = black
        self.gridspace = self.gridspace
        xs = self.location[0]
        xe = xs + self.size[0]
        ys = self.location[1]
        ye = ys + self.size[1]
        self.screen.set_clip(xs,ys, xe, ye)
        # draw vertical grid lines
        ys = self.location[1]
        ye = ys + self.size[1]
        for i in range(int(self.size[0]/self.gridspace)):
            gx = self.gridspace * i
            # starting pont
            xs = self.location[0] + gx
            # ending pont
            xe = xs
            pygame.draw.line(self.screen, gridcolor, (xs, ys), (xe, ye), 1)

        # draw horizontal grid lines
        xs = self.location[0]
        xe = xs + self.size[0]
        for i in range(int(self.size[1]/self.gridspace)):
            gy = self.gridspace * i
            # starting pont
            ys = self.location[1] + gy
            # ending pont
            ye = ys
            pygame.draw.line(self.screen, gridcolor, (xs, ys), (xe, ye), 1)
        self.screen.set_clip()

    def border(self):
        gridcolor = black
        xs = self.location[0]
        ys = self.location[1]
        xe = self.location[0] + self.size[0]
        ye = self.location[1] + self.size[1]

        pygame.draw.line(self.screen, gridcolor, (xs, ys), (xe, ys), 3)
        pygame.draw.line(self.screen, gridcolor, (xs, ye), (xe, ye), 3)
        pygame.draw.line(self.screen, gridcolor, (xs, ys), (xs, ye), 3)
        pygame.draw.line(self.screen, gridcolor, (xe, ys), (xe, ye), 3)

    def clear(self):
        # clear trend
        xs = self.location[0]
        ys = self.location[1]
        xe = self.size[0]
        ye = self.size[1]
        pygame.draw.rect(self.screen, dkgrey, (xs,ys, xe, ye), 0)

    def penupdate(self):
        self.clear()
        # redraw the grid
        self.grid()

        # plot a point on the trend for each value stored in pvval, spval, mvval
        self.pvval.pop(0)
        self.pvval.append(self.loop.pv)
        pvvalmax = self.loop.pvmax
        for i, pvval in enumerate(self.pvval[:-1]):
            xs = self.location[0] + (i / len(self.pvval) * self.size[0])
            xe = self.location[0] + ((i + 1) / len(self.pvval) * self.size[0])
            if pvvalmax > 0:
                ys = (self.location[1] + self.size[1]) - ((pvval / pvvalmax) * self.size[1])
            else:
                ys = self.location[1] + self.size[1]
            if pvvalmax > 0:
                ye = (self.location[1] + self.size[1]) - ((self.pvval[i+1] / pvvalmax) * self.size[1])
            else:
                ye = self.location[1] + self.size[1]
            if pvval > 0:
                pygame.draw.line(self.screen, red, (xs, ys), (xe, ye), 1)

        # plot a point on the trend for each value stored in pvval, spval, mvval
        self.spval.pop(0)
        self.spval.append(self.loop.sp)
        spvalmax = self.loop.pvmax
        for i, spval in enumerate(self.spval[:-1]):
            xs = self.location[0] + (i / len(self.spval) * self.size[0])
            xe = self.location[0] + ((i + 1) / len(self.spval) * self.size[0])
            if spvalmax > 0:
                ys = (self.location[1] + self.size[1]) - ((spval / spvalmax) * self.size[1])
            else:
                ys = self.location[1] + self.size[1]
            if spvalmax > 0:
                ye = (self.location[1] + self.size[1]) - ((self.spval[i+1] / spvalmax) * self.size[1])
            else:
                ye = self.location[1] + self.size[1]
            if spval > 0:
                pygame.draw.line(self.screen, green, (xs, ys), (xe, ye), 1)

        # plot a point on the trend for each value stored in pvval, spval, mvval
        self.mvval.pop(0)
        self.mvval.append(self.loop.mv)
        mvvalmax = 100
        for i, mvval in enumerate(self.mvval[:-1]):
            xs = self.location[0] + (i / len(self.mvval) * self.size[0])
            xe = self.location[0] + ((i + 1) / len(self.mvval) * self.size[0])
            if mvvalmax > 0:
                ys = (self.location[1] + self.size[1]) - ((mvval / mvvalmax) * self.size[1])
            else:
                ys = self.location[1] + self.size[1]
            if mvvalmax > 0:
                ye = (self.location[1] + self.size[1]) - ((self.mvval[i+1] / mvvalmax) * self.size[1])
            else:
                ye = self.location[1] + self.size[1]
            if mvval > 0:
                pygame.draw.line(self.screen, white, (xs, ys), (xe, ye), 1)


        # redraw the border
        self.border()

class Tank:
    # a tank with a drain valve
    def __init__(self, screen, filename, location):
        self.image0 = pygame.image.load(filename)
        self.initimage = self.image0
        self.image = self.initimage
        self.rect = self.image.get_rect()
        self.rect.x = location[0]
        self.rect.y = location[1]

        scalefact = 0.5
        b_width = int(scalefact * self.rect[2])
        b_height = int(scalefact * self.rect[3])
        self.image = pygame.transform.scale(self.initimage, (b_width, b_height))
        self.screen = screen

    def draw(self):
        self.screen.blit(self.image, self.rect)

class Thumbwheel:
    # a three digit indicator with up and down buttons
    def __init__(self, screen, filename1, location, yrange, mag, eu = '%', desc = ''):
        self.yrange = yrange
        self.location = location
        self.size = (b_width * 3 + 8, b_height * 3)
        xs = location[0]
        ys = location[1]
        xe = self.size[0]
        ye = self.size[1]
        self.rectangle = (xs, ys, xe, ye)
        self.screen = screen
        self.desc = desc
        self.mag = mag
        # mag should be a power of 10
        # 1 gives units buttons
        # 10 gives 10s buttons
        # 100 gives 100s buttons
        # draw a container rectangle with buttons
        self.faceplate = pygame.draw.rect(self.screen, dkgrey, self.rectangle)
        if mag >= 1:
            self.button1 = Button(self.screen, filename1)
            self.button1.rect.x = xs + 2 + 2 * (b_width + 2)
            self.button1.rect.y = 1 + ys
            self.button1.draw()
        if mag >= 10:
            self.button2 = Button(self.screen, filename1)
            self.button2.rect.x = xs + 2 + b_width + 2
            self.button2.rect.y = self.button1.rect.y
            self.button2.draw()
        if mag >= 100:
            self.button3 = Button(self.screen, filename1)
            self.button3.rect.x = xs + 2  
            self.button3.rect.y = self.button1.rect.y
            self.button3.draw()
        # downys
        if mag >= 1:
            self.button4 = Button(self.screen, filename1, 'down')
            self.button4.rect.x = self.button1.rect.x
            self.button4.rect.y = ys + b_height * 2
            self.button4.draw()
        if mag >= 10:
            self.button5 = Button(self.screen, filename1, 'down')
            self.button5.rect.x = self.button2.rect.x
            self.button5.rect.y = self.button4.rect.y
            self.button5.draw()
        if mag >= 100:
            self.button6 = Button(self.screen, filename1, 'down')
            self.button6.rect.x = self.button3.rect.x
            self.button6.rect.y = self.button4.rect.y
            self.button6.draw()
        # value display
        self.myval = NumericVariable(screen, (xs, ys, b_width, b_height), eu, desc)
        self.myval.draw(0)

    def buttoncheck(self, location, val):
        if self.mag >= 1 and self.button1.checkcollide(location):
            val += 1
        elif self.mag >= 10 and self.button2.checkcollide(location):
            val += 10
        elif self.mag >= 100 and self.button3.checkcollide(location):
            val += 100
        elif self.mag >= 1 and self.button4.checkcollide(location):
            val -= 1
        elif self.mag >= 10 and self.button5.checkcollide(location):
            val -= 10
        elif self.mag >= 100 and self.button6.checkcollide(location):
            val -= 100
        # limit to range
        val = between(self.yrange[0], val, self.yrange[1])
        self.myval.draw(val)
        return val

class NumericVariable:
    def __init__(self, screen, location, eu = '', desc = ''):
        self.screen = screen
        self.location = (location[0], location[1] + b_height)
        self.size = (b_width * 3 + 8, b_height)
        self.text = str(100)
        self.eu = eu
        self.desc = desc

    def draw(self, x):
        self.clear()
        ptext.draw(str(x), (self.location[0] + int(1 * b_width), 
            self.location[1] + 5), fontsize=32, align="right", color="black")
        if self.eu:
            ptext.draw(self.eu, (self.location[0] + self.size[0] + 5, 
                self.location[1] + 5), fontsize=32, align="right", color="black")
        if self.desc:
            ptext.draw(self.desc, (self.location[0] - 30, 
                self.location[1] + 5), fontsize=32, align="right", color="black")

    def clear(self):
        # clear trend
        xs = self.location[0]
        ys = self.location[1]
        xe = self.size[0]
        ye = self.size[1]
        pygame.draw.rect(self.screen, dkgrey, (xs,ys, xe, ye), 0)

class Button:
    def __init__(self, screen, filename, updown = 'up', 
        b_width = b_width, b_height = b_height):
        self.image0 = pygame.image.load(filename)
        if updown == 'up':
            self.initimage = self.image0
        else:
            self.initimage = pygame.transform.rotate(self.image0, 180)
        self.image = pygame.transform.scale(self.initimage, (b_width, b_height))
        self.rect = self.image.get_rect()
        self.screen = screen

    def checkcollide(self, location):
        if self.rect.collidepoint(location):
            return True
        return False

    def draw(self):
        self.screen.blit(self.image, self.rect)

    def clear(self):
        # clear handy help
        pygame.draw.rect(self.screen, grey, self.rect, 0)

class PID:
    def __init__(self, tag = 'FIC100', pvmin = 0, pvmax = 100, eu = "GPM", desc = 'None'):
        # base class of PID control loop
        # range minimmum
        # range maximum
        # engineering units
        # default = 0 - 100 GPM
        self.tag = tag
        self.pvmin = pvmin
        self.pvmax = pvmax
        self.eu = eu
        self.pv = pvmin
        self.sp = (pvmax - pvmin) / 2
        self.mv = 0
        self.desc = desc
        self.P = 0
        self.I = 0
        self.last_dev = 0

    def PID_calc(self):
        # direct acting control
        # when pv increases, mv increases
        dev = self.pv - self.sp
        dev_change = dev - self.last_dev
        proportionalaction = self.P * dev_change
        integral_action = dev * self.I
        new_mv = self.mv + (proportionalaction + integral_action) / 100
        self.mv = between(0, new_mv, 100)
        print(integral_action)

    def increase(self, delta):
        # the pv increases by an amaount delta
        self.pv += delta
        # clamp at range limits
        self.pv = between(self.pvmin, self.pv, self.pvmax)
        # limit precision to 100ths
        self.pv = r(self.pv, 0.01)

def r(x, precision = 0.01):
    # round to precision places
    if precision == 1:
        return int(x)
    return round(x * (1 / precision)) / (1 / precision)

def between(lower, v, upper):
    # limit v between lower and upper
    return max(min(upper, v), lower)

if __name__ == "__main__":
    print('classlib.py Class Library')
