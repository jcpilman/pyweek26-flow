# pyweek26 entry 
# FLOW
# help library

import pygame
import math
import time
from . import ptext

# color pallet
grey = (200, 200, 200)
dkgrey = (150, 150, 150)
white = (255, 255, 255)
black = (0, 0, 0)
purple = (200, 0, 200)
ltblue = (100, 250, 240)
red = (255,0,0)
green = (0,255,0)
blue = (0,0,255)
dkblue = (0,0,128)
pink = (255,200,200)

datapath = 'data/'

def clickhelp(size, x):
    if x == 0: 
        textx = int(size[0] * 0.25)
        texty = int(size[1] * 0.55)
        fontsize = 128
        ptext.draw("FLOW WOLF", 
            (textx, texty), fontsize=fontsize, align="right", color="white")

    if x == 1:
        textx = int(size[0] * 0.15)
        texty = int(size[1] * 0.15)

        fontsize = 32
        htext = "Hi. Today you are going to control flow in an industrial plant."
        ptext.draw(htext, (textx, texty), fontsize=fontsize, align="right", color="black")
        texty += fontsize
        htext = "If you do well, you may be qualified for more than a pyweek prize."
        ptext.draw(htext, (textx, texty), fontsize=fontsize, align="right", color="black")
        texty += 2 * fontsize
        htext = "Do you see Handy up in the corner?"
        ptext.draw(htext, (textx, texty), fontsize=fontsize, align="right", color="black")
        texty += fontsize
        htext = "Sometimes if you click on him, he will give you a hint."
        ptext.draw(htext, (textx, texty), fontsize=fontsize, align="right", color="black")
        texty += 2 * fontsize
        htext = "One more thing. Your progress is saved in 'savedgame.txt'."
        ptext.draw(htext, (textx, texty), fontsize=fontsize, align="right", color="black")
        texty += fontsize
        htext = "If you want to start back at the beginning, delete that file."
        ptext.draw(htext, (textx, texty), fontsize=fontsize, align="right", color="black")
        texty += 2 * fontsize
        htext = "Mouse click to continue. ESC key to quit."
        ptext.draw(htext, (textx, texty), fontsize=fontsize, align="right", color="black")
    if x == 2:
        textx = int(size[0] * 0.15)
        texty = int(size[1] * 0.15)
        fontsize = 32
        htext = "Step one:"
        ptext.draw(htext, (textx, texty), fontsize=fontsize, align="right", color="black")
        texty += fontsize
        htext = "The small triangles above and below the numeric display"
        ptext.draw(htext, (textx, texty), fontsize=fontsize, align="right", color="black")
        texty += fontsize
        htext = "are up and down arrows."
        ptext.draw(htext, (textx, texty), fontsize=fontsize, align="right", color="black")
        texty += fontsize
        htext = "Click the arrows to change the value to 99"
        ptext.draw(htext, (textx, texty), fontsize=fontsize, align="right", color="black")
    if x == 3:
        textx = int(size[0] * 0.15)
        texty = int(size[1] * 0.50)
        fontsize = 64
        htext = "Good Job"
        ptext.draw(htext, (textx, texty), fontsize=fontsize, align="right", color="red")
        texty += fontsize
        fontsize = 32
        htext = "Mouse click Continue to continue. ESC key to quit."
        ptext.draw(htext, (textx, texty), fontsize=fontsize, align="right", color="black")
    if x == 4:
        textx = int(size[0] * 0.07)
        texty = int(size[1] * 0.15)
        fontsize = 32
        htext = "Open the valve to lower the tank level."
        ptext.draw(htext, (textx, texty), fontsize=fontsize, align="right", color="black")
        texty += fontsize
        htext = "When it gets below 25%, close the valve."
        ptext.draw(htext, (textx, texty), fontsize=fontsize, align="right", color="black")

    if x == 5:
        textx = int(size[0] * 0.15)
        texty = int(size[1] * 0.10)
        fontsize = 64
        htext = "Nice!"
        ptext.draw(htext, (textx, texty), fontsize=fontsize, align="right", color="red")
        texty += fontsize
        fontsize = 32
        htext = "You drained the tank MANUALLY. Now you must control the tank level"
        ptext.draw(htext, (textx, texty), fontsize=fontsize, align="right", color="black")
        texty += fontsize
        htext = "AUTOMATICALLY. Handy already put the controller in Automatic mode for you."
        ptext.draw(htext, (textx, texty), fontsize=fontsize, align="right", color="black")
        texty += fontsize
        htext = "Try controlling the level to a Setpoint (SP) of 50%."
        ptext.draw(htext,(textx, texty), fontsize=fontsize, align="right", color="black")
        texty += 2 * fontsize
        htext = "Mouse click Continue to continue. ESC key to quit."
        ptext.draw(htext, (textx, texty), fontsize=fontsize, align="right", color="black")

    if x == 6:
        textx = int(5)
        texty = int(5)
        fontsize = 24

        htext = "Try controlling the level to a Setpoint (SP) of 50%."
        ptext.draw(htext,(textx, texty), fontsize=fontsize, align="right", color="black")
        texty += fontsize
        htext = "'Tune' the Automatic control parameters P and I."
        ptext.draw(htext,(textx, texty), fontsize=fontsize, align="right", color="black")
        texty += fontsize
        htext = "P is the proportional gain. Start by increasing this number."
        ptext.draw(htext, (textx, texty), fontsize=fontsize, align="right", color="black")
        texty += fontsize
        htext = "I is the integral (in repeats per second). Here is a hint: the tank level is"
        ptext.draw(htext, (textx, texty), fontsize=fontsize, align="right", color="black")
        texty += fontsize
        htext = "an integrating process (it adds level) so don't use too much (I)."
        ptext.draw(htext, (textx, texty), fontsize=fontsize, align="right", color="black")

        texty += fontsize
        htext = ""
        ptext.draw(htext, (textx, texty), fontsize=fontsize, align="right", color="black")
        texty += fontsize
        htext = "Good Luck."
        ptext.draw(htext, (textx, texty), fontsize=fontsize, align="right", color="black")

    if x == 99:
        textx = int(size[0] * 0.15)
        texty = int(size[1] * 0.50)
        fontsize = 64
        htext = "Congratulations!"
        ptext.draw(htext, (textx, texty), fontsize=fontsize, align="right", color="red")
        texty += fontsize
        fontsize = 32
        htext = "You controlled the level by controlling the flow."
        ptext.draw(htext, (textx, texty), fontsize=fontsize, align="right", color="black")

        texty += fontsize
        htext = "You are our new expert."
        ptext.draw(htext, (textx, texty), fontsize=fontsize, align="right", color="black")

        texty += fontsize
        htext = "Thanks for playing."
        ptext.draw(htext, (textx, texty), fontsize=fontsize, align="right", color="black")

handyx = 10
handyy = 768 - 32
handyfont = 32

def handyhelp(screen, context=0):
    # display help when handy get clicked
    # sorry, I'm hard coding the location, I'm running out of time
    # screen_size = (1028, 768)
    handyclear(screen)
    if context == 0:
        htext = 'If I had help for you it would appear here. Try clicking free space to continue.'
    elif context == 1:
        htext = 'If I had help for you it would appear here. Try clicking free space to continue.'
    elif context == 2:
        htext = 'Those arrows are for 100s, 10s, and 1s.'
    elif context == 40:
        htext = "The inflow keeps changing. That doesn't make things easier on you."
        context += 1
    elif context == 41:
        htext = 'Let the level get below 25 before closing the valve.'
        context += 1
    elif context == 42:
        htext = 'Did you figure out this Trend yet?'
        context += 1
    elif context == 43:
        htext = 'The red pen is the Tank Level in percent.'
        context += 1
    elif context == 44:
        htext = 'The white pen shows the valve position in percent of full open.'
        context += 1
    elif context == 45:
        htext = 'The green pen shows the tank level setpoint, currently 25%.'
        context = 40
    elif context == 50:
        htext = 'You will be done when those trend lines get a little smoother.'
        context += 1
    elif context == 51:
        htext = 'Higher Proportional Gain makes the valve move faster when the level moves away from setpoint.'
        context += 1
    elif context == 52:
        htext = 'Higher integral (in Repeats per Second) moves the valve more over time when the level stays away from setpoint.'
        context += 1
    elif context == 53:
        htext = 'You will be done when those trend lines get a little smoother.'
        context += 1
    elif context == 54:
        htext = 'You will be done when those trend lines get a little smoother.'
        context += 1
    elif context == 55:
        htext = 'You will be done when those trend lines get a little smoother.'
        context = 50
    else:
        htext = "I'm speechless."
    ptext.draw(htext, (handyx, handyy), fontsize=handyfont, color="black")
    return context


def handyclear(screen):
    # clear handy help
    pygame.draw.rect(screen, dkgrey, (handyx,handyy, 1028, 768), 0)

if __name__ == "__main__":
    print('helptext.py is a Library')
